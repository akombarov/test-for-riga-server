FROM golang:1.11.2
RUN mkdir /app
RUN mkdir /go/src/test-in
ADD . /go/src/test-in
WORKDIR /go/src/test-in

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app

FROM alpine:latest

RUN apk add --no-cache --update ca-certificates tzdata \
    && cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime \
    && echo "Europe/Moscow" >  /etc/timezone

RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/test-in/app .
ENV FILE_PATH=/root/data.json
CMD ["/root/app"]
