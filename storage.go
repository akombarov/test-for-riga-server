package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"time"
)

type Storage struct {
	sync.RWMutex
	Map map[string]*ValueAndTtl
}

type ValueAndTtl struct {
	Value string
	Ttl   int64
}

type DiskData struct {
	Data []Data `json:"data"`
}

type Data struct {
	Key   string
	Value string
	Ttl   int64
}

func (s *Storage) put(key string, value string, ttl int64) {
	s.Lock()
	defer s.Unlock()
	v := &ValueAndTtl{
		Value: value,
		Ttl:   ttl,
	}
	s.Map[key] = v

}

func (s *Storage) read(key string) string {
	s.RLock()
	defer s.RUnlock()
	if val, ok := s.Map[key]; ok {
		return val.Value
	}
	return "key not found"
}

func (s *Storage) delete(key string) {
	s.Lock()
	defer s.Unlock()
	delete(s.Map, key)
}

func (s *Storage) checkTtl() {
	for k, v := range s.Map {
		if v.Ttl <= time.Now().Unix() {
			s.delete(k)
		}
	}
}

func (s *Storage) saveToDisk() bool {
	data := `{"data": [`
	for k, v := range s.Map {
		data += `{"key":"` + k + `","value":"` + v.Value + `"` + `,` + `"ttl":` + fmt.Sprint(v.Ttl) + `}` + `,`
	}
	data = data[:len(data)-1] + `]}`

	err := ioutil.WriteFile(envVariable("FILE_PATH"), []byte(data), 0644)
	if err != nil {
		log.Println("(saveToDisk)error:", err)
		return false
	}
	return true
}

func (s *Storage) loadFromDisk() bool {
	data, err := ioutil.ReadFile(envVariable("FILE_PATH"))
	if err != nil {
		log.Println("(getFromDisk)error:", err)
		return false
	}
	var d DiskData
	err = json.Unmarshal(data, &d)
	if err != nil {
		log.Println("(getFromDisk)error:", err)
		return false
	}
	mapa := make(map[string]*ValueAndTtl)
	for _, v := range d.Data {
		val := &ValueAndTtl{
			Value: v.Value,
			Ttl:   v.Ttl,
		}
		mapa[v.Key] = val
	}
	s.Lock()
	defer s.Unlock()
	s.Map = mapa
	return true
}
