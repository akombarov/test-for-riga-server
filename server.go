package main

import (
	"bufio"
	"encoding/json"
	"log"
	"net"
	"sync"
	"time"
)

type Server struct {
	Addr         string
	IdleTimeout  time.Duration
	MaxReadBytes int64

	listener   net.Listener
	conns      map[*conn]struct{}
	mu         sync.Mutex
	inShutdown bool
}

func (srv *Server) ListenAndServe() error {
	addr := srv.Addr
	if addr == "" {
		addr = ":8080"
	}
	log.Printf("starting server on %v\n", addr)
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	defer listener.Close()
	srv.listener = listener
	for {
		// should be guarded by mu
		if srv.inShutdown {
			break
		}
		newConn, err := listener.Accept()
		if err != nil {
			log.Printf("error accepting connection %v", err)
			continue
		}
		log.Printf("accepted connection from %v", newConn.RemoteAddr())
		conn := &conn{
			Conn:          newConn,
			IdleTimeout:   srv.IdleTimeout,
			MaxReadBuffer: srv.MaxReadBytes,
		}
		srv.trackConn(conn)
		conn.SetDeadline(time.Now().Add(conn.IdleTimeout))
		go srv.handle(conn)
	}
	return nil
}

func (srv *Server) trackConn(c *conn) {
	defer srv.mu.Unlock()
	srv.mu.Lock()
	if srv.conns == nil {
		srv.conns = make(map[*conn]struct{})
	}
	srv.conns[c] = struct{}{}
}

func (srv *Server) handle(conn *conn) error {
	defer func() {
		log.Printf("closing connection from %v", conn.RemoteAddr())
		conn.Close()
		srv.deleteConn(conn)
	}()

	r := bufio.NewReader(conn)
	w := bufio.NewWriter(conn)
	scanr := bufio.NewScanner(r)

	sc := make(chan bool)
	conn.SetReadDeadline(time.Now().Add(time.Minute * 10))
	for {
		go func(s chan bool) {
			s <- scanr.Scan()
		}(sc)
		select {
		case scanned := <-sc:
			if !scanned {
				if err := scanr.Err(); err != nil {
					return err
				}
				return nil
			}
			var msg Message
			err := json.Unmarshal(scanr.Bytes(), &msg)
			if err != nil {
				response := `{` + `"status":` + `"` + err.Error() + `"` + `}` + `\n`
				w.WriteString(response)
				w.Flush()
				conn.SetReadDeadline(time.Now().Add(time.Minute * 10))
				log.Println("(handle)error, msg:", scanr.Text())
				continue
			}

			status := msg.checkMsg()
			response := `{` + `"response":` + `"` + status + `"` + `}`

			w.WriteString(response + "\n")
			w.Flush()

			conn.SetReadDeadline(time.Now().Add(time.Minute * 10))

		}
	}
}

type Message struct {
	Action string `json:"action"`
	Key    string `json:"key"`
	Value  string `json:"value"`
	Ttl    int64  `json:"ttl"`
}

func (m *Message) checkMsg() string {
	switch m.Action {
	case "put":
		st.put(m.Key, m.Value, m.Ttl)
		return "put ok"
	case "read":
		resp := st.read(m.Key)
		return resp
	case "delete":
		st.delete(m.Key)
		return "initiated removal"
	case "save":
		ok := st.saveToDisk()
		if !ok {
			return "error -  details in server logs"
		}
		return "saving was successful"
	case "load":
		ok := st.loadFromDisk()
		if !ok {
			return "error -  details in server logs"
		}
		return "loading was successful"
	default:
		return "unknown command"
	}
}

func (srv *Server) deleteConn(conn *conn) {
	defer srv.mu.Unlock()
	srv.mu.Lock()
	delete(srv.conns, conn)
}

func (srv *Server) Shutdown() {
	srv.inShutdown = true
	log.Println("shutting down...")
	srv.listener.Close()
	ticker := time.NewTicker(500 * time.Millisecond)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			log.Printf("waiting on %v connections", len(srv.conns))
		}
		if len(srv.conns) == 0 {
			return
		}
	}
}
