package main

import (
	"fmt"
	"log"
	"os"
	"time"
)

var st Storage

var envVariableData = map[string]string{
	"FILE_PATH": "",
}

func main() {
	t := time.Now()
	log.Println("t:", t)
	err := initEnv()
	if err != nil {
		log.Printf("Can't find some ENV: %s", err.Error())
		return
	}
	m := make(map[string]*ValueAndTtl)
	st.Map = m
	go func() {
		for {
			st.checkTtl()
		}
	}()

	srv := Server{
		Addr:         ":8080",
		IdleTimeout:  60 * time.Minute,
		MaxReadBytes: 10000,
	}
	srv.ListenAndServe()
}

func envVariable(key string) string {
	return envVariableData[key]
}

func initEnv() error {
	for k := range envVariableData {
		val, exists := os.LookupEnv(k)
		if !exists || val == "" {
			return fmt.Errorf("error: %s not found", k)
		}
		envVariableData[k] = val
	}
	return nil
}
